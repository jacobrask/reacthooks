import { useEffect } from 'react'

export default function useDocumentTitle(title) {

  useEffect(() => {
    document.title = String(title)
  }, [ title ])

}
