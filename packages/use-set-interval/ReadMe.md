<div align="center">
    <p>
      <a href="https://reacthooks.org/use-set-interval"><img src="https://i.postimg.cc/SRRmYPpP/logo-on-trans.png" /></a>
    </p>
    <h2>
      <a href="https://reacthooks.org/use-set-interval"><span style="color: yellow;">✴</span> Project Homepage : Documentation and Examples <span style="color: yellow;">✴</span></a>
    </h2>
</div>

# useSetInterval #

A React Hook (using useEffect) which uses window.setInterval().

Note: this was published and didn't work a few days before Dan published this
[Making setInterval Declarative with React Hooks](https://overreacted.io/making-setinterval-declarative-with-react-hooks/),
which saved my bacon. Many thanks.

## Synopsis ##

In your React component, increment a counter every second:

```
import { useState } from 'react'
import useSetInterval from "use-set-interval"

function SetInterval() {
  const [ count, setCount ] = useState(0)

  useSetInterval(() => setCount(count + 1), 1000)

  return <p>Count = { count }</p>
}
```

There is no need to keep a reference to the interval ID since it will be automatically cleared when the component unmounts.

Please note that the interval is cleared and reset if you change the interval `ms` but not if you change the function `fn`. This is a good thing, since it allows you to change the interval in a declarative way, instead of having to fiddle with the interval IDs yourself.

## Usage ##

```
useSetInterval(fn, delay)
```

* `fn` is a function to execute
* `delay` is the delay between executions in milliseconds

## Other Hooks ##

Please see all of my other React Hooks:

* [use-document-title](https://www.npmjs.com/package/use-document-title) - Change the `document.title`
* [use-resize](https://www.npmjs.com/package/use-resize) - Subscribe to Window 'resize' events and get the width and height
* [use-window-width](https://www.npmjs.com/package/use-window-width) - Subscribe to Window 'resize' events and get the width
* [use-online](https://www.npmjs.com/package/use-online) - Get online/offline status
* [use-match-media](https://www.npmjs.com/package/use-match-media) - Get whether a media query is matched
  * `.usePrefersColorScheme()` - Get whether the user prefers the 'light' or 'dark' color scheme
* [use-set-timeout](https://www.npmjs.com/package/use-set-timeout) - use and automatically clear a `setTimeout()`
* [use-set-interval](https://www.npmjs.com/package/use-set-interval) - use and automatically clear a `setInterval()`
* [use-orientation-change](https://www.npmjs.com/package/use-orientation-change) - get Device Orientation updates
* [use-session-storage](https://www.npmjs.com/package/use-session-storage) - gets and sets a key in window.sessionStorage

## Author ##

```
$ npx chilts

   ╒════════════════════════════════════════════════════╕
   │                                                    │
   │   Andrew Chilton (Personal)                        │
   │   -------------------------                        │
   │                                                    │
   │          Email : andychilton@gmail.com             │
   │            Web : https://chilts.org                │
   │        Twitter : https://twitter.com/andychilton   │
   │         GitHub : https://github.com/chilts         │
   │         GitLab : https://gitlab.org/chilts         │
   │                                                    │
   │   Apps Attic Ltd (My Company)                      │
   │   ---------------------------                      │
   │                                                    │
   │          Email : chilts@appsattic.com              │
   │            Web : https://appsattic.com             │
   │        Twitter : https://twitter.com/AppsAttic     │
   │         GitLab : https://gitlab.com/appsattic      │
   │                                                    │
   │   Node.js / npm                                    │
   │   -------------                                    │
   │                                                    │
   │        Profile : https://www.npmjs.com/~chilts     │
   │           Card : $ npx chilts                      │
   │                                                    │
   ╘════════════════════════════════════════════════════╛

```

(Ends)
