import React, { useEffect, useRef } from 'react'

// Code : https://overreacted.io/making-setinterval-declarative-with-react-hooks/
export default function useSetInterval(fn, delay) {
  const savedFn = useRef()

  // Remember the latest callback function.
  useEffect(() => {
    savedFn.current = fn
  }, [ fn ])

  // Set up the interval.
  useEffect(() => {
    function tick() {
      savedFn.current()
    }
    if (delay !== null) {
      const id = setInterval(tick, delay)
      return () => clearInterval(id)
    }
  }, [ delay ])

}
