<div align="center">
    <p>
      <a href="https://reacthooks.org/use-resize"><img src="https://i.postimg.cc/SRRmYPpP/logo-on-trans.png" /></a>
    </p>
    <h2>
      <a href="https://reacthooks.org/use-resize"><span style="color: yellow;">✴</span> Project Homepage : Documentation and Examples <span style="color: yellow;">✴</span></a>
    </h2>
</div>

# useResize #

React (State and Effect) Hook to get your window size.

## Synopsis ##

In your React component:

```
import useResize from "use-resize"

function SizeInfo() {
  const size = useResize()

  return <p>size = ({ size.width }, { size.height })</p>
}
```

There is no need to subscribe or unsubscribe to the window `'resize'` event
since this hook does it for you.

## Other Hooks ##

Please see all of my other React Hooks:

* [use-document-title](https://www.npmjs.com/package/use-document-title) - Change the `document.title`
* [use-resize](https://www.npmjs.com/package/use-resize) - Subscribe to Window 'resize' events and get the width and height
* [use-window-width](https://www.npmjs.com/package/use-window-width) - Subscribe to Window 'resize' events and get the width
* [use-online](https://www.npmjs.com/package/use-online) - Get online/offline status
* [use-match-media](https://www.npmjs.com/package/use-match-media) - Get whether a media query is matched
  * `.usePrefersColorScheme()` - Get whether the user prefers the 'light' or 'dark' color scheme
* [use-set-timeout](https://www.npmjs.com/package/use-set-timeout) - use and automatically clear a `setTimeout()`
* [use-set-interval](https://www.npmjs.com/package/use-set-interval) - use and automatically clear a `setInterval()`
* [use-orientation-change](https://www.npmjs.com/package/use-orientation-change) - get Device Orientation updates
* [use-session-storage](https://www.npmjs.com/package/use-session-storage) - gets and sets a key in window.sessionStorage

## Author ##

```
$ npx chilts

   ╒════════════════════════════════════════════════════╕
   │                                                    │
   │   Andrew Chilton (Personal)                        │
   │   -------------------------                        │
   │                                                    │
   │          Email : andychilton@gmail.com             │
   │            Web : https://chilts.org                │
   │        Twitter : https://twitter.com/andychilton   │
   │         GitHub : https://github.com/chilts         │
   │         GitLab : https://gitlab.org/chilts         │
   │                                                    │
   │   Apps Attic Ltd (My Company)                      │
   │   ---------------------------                      │
   │                                                    │
   │          Email : chilts@appsattic.com              │
   │            Web : https://appsattic.com             │
   │        Twitter : https://twitter.com/AppsAttic     │
   │         GitLab : https://gitlab.com/appsattic      │
   │                                                    │
   │   Node.js / npm                                    │
   │   -------------                                    │
   │                                                    │
   │        Profile : https://www.npmjs.com/~chilts     │
   │           Card : $ npx chilts                      │
   │                                                    │
   ╘════════════════════════════════════════════════════╛

```

(Ends)
