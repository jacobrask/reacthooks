import React, { useState, useEffect } from 'react'

export default function useSessionStorage(key, val) {
  const [ value, setValue ] = useState(val || window.sessionStorage.getItem(key))

  useEffect(() => {
    window.sessionStorage.setItem(key, value)
  }, [ value ])

  return [ value, setValue ]
}
